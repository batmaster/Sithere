/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import {
  StackNavigator,
} from 'react-navigation';

import HomeScreen from './components/screens/HomeScreen';
import DiscoverScreen from './components/screens/DiscoverScreen';
import TestScreen from './components/screens/TestScreen';

console.disableYellowBox = true;

const App = StackNavigator({
    // TestScreen: { screen: TestScreen },
    HomeScreen: { screen: HomeScreen },
    // DiscoverScreen: { screen: DiscoverScreen }
});

export default App;
