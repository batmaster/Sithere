import React, { Component } from 'react';
import { AppRegistry, ActivityIndicator, TextInput, View, StyleSheet, Image, Dimensions } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Text } from 'native-base';
import { StackNavigator } from 'react-navigation';




import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import MapTab from '../../components/screens/home/MapTab';
import PlaceCarousel from '../../components/PlaceCarousel';



export default class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Home',
        // header: null
    };

    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            routes: [
                { key: 'map', title: 'Map' },
                { key: 'story', title: 'Story' },
                { key: 'profile', title: 'Profile' },
                { key: 'setting', title: 'Setting' }
            ],
        };

    }

    componentWillMount() {

    }

    componentDidMount() {

    }



    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={{flex: 1}}>
                <TabView
                    navigationState={this.state}
                    renderScene={SceneMap({
                        map: MapTab,
                        story: MapTab,
                        profile: MapTab,
                        setting: MapTab,
                    })}
                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                    tabBarPosition={'bottom'}
                />
            </View>
        );
    }


}

const styles = {
    screen: {
        flex: 1,
    },
};
