import React, { Component } from 'react';
import { AppRegistry, ActivityIndicator, TextInput, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Form, Item, Label, Input, Picker, Icon, Textarea } from 'native-base';
import { StackNavigator } from 'react-navigation';


import DateTimePicker from 'react-native-modal-datetime-picker';


import RNPickerSelect from 'react-native-picker-select';

import MapView, { Marker } from 'react-native-maps';
import firebase from 'react-native-firebase';

import moment from 'moment';

import PlaceCarousel from '../../components/PlaceCarousel';

import ImagePicker from 'react-native-image-picker';

import ImageResizer from 'react-native-image-resizer';


export default class DiscoverScreen extends Component {
    static navigationOptions = {
        title: 'Discover',
        // header: null
    };

    constructor(props) {
        super(props);

        this.inputRefs = {};

        this.state = {
            openTimeOptions: [
                {
                    label: '24Hrs',
                    value: '00000000',
                },
                {
                    label: '08:00-20:00',
                    value: '08002000',
                },
                {
                    label: 'Custom',
                    value: 'custom',
                },
            ],
            isDateTimePickerFromVisible: false,
            isDateTimePickerToVisible: false,

            typeOptions: [
                {
                    label: 'Free',
                    value: 'free',
                },
                {
                    label: 'Charged',
                    value: 'charged',
                },
                {
                    label: 'Buy product & Free using',
                    value: 'buy',
                },
            ],

            wifiOptions: [
                {
                    label: 'Free',
                    value: 'free',
                },
                {
                    label: 'Charged',
                    value: 'charged',
                },
                {
                    label: 'Buy product & Free using',
                    value: 'buy',
                },
            ],

            parkingOptions: [
                {
                    label: 'Free',
                    value: 'free',
                },
                {
                    label: 'Charged',
                    value: 'charged',
                },
                {
                    label: 'Buy product & Free using',
                    value: 'buy',
                },
            ],

            marker: this.props.navigation.state.params.current,

            title: '',
            openTime: '00000000',
            from: undefined,
            fromString: '',
            to: undefined,
            toString: '',

            type: 'free',

            wifi: 'free',

            parking: 'free',

            description: '',
        };

        this.options = {
            title: 'Select Avatar',
            customButtons: [
                {name: 'fb', title: 'Choose Photo from Facebook'},
            ],
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };



    }

    componentWillMount() {
        var config = {
            apiKey: "AIzaSyDsll6qnvvoD2QqyVYomTP65ZOGWW5-qUQ",
            authDomain: "sithere-5db41.firebaseapp.com",
            databaseURL: "https://sithere-5db41.firebaseio.com",
            projectId: "sithere-5db41",
            storageBucket: "sithere-5db41.appspot.com",
            messagingSenderId: "872587675566"
          };
          firebase.initializeApp(config);


          firebase.database().ref('/locations').once('value').then(function(snapshot) {
              console.log(snapshot);
          });

    }

    componentDidMount() {
        this.map.animateToCoordinate(this.state.marker);
    }

    onValueChange2(value: string) {
        this.setState({
            openTime: value
        });

        if (value == 'custom') {

        }
    }

    writeNewDiscovery() {
        console.log(this.state);

        var openTime = this.state.openTime == 'custom' ? this.state.fromString + this.state.toString : this.state.openTime;
        var postData = {
            latitude: this.state.marker.latitude,
            longitude: this.state.marker.longitude,

            title: this.state.title,
            openTime: openTime,
            type: this.state.type,
            wifi: this.state.wifi,
            parking: this.state.parking,
            description: this.state.description
        };

        var newPostKey = firebase.database().ref().child('posts').push().key;

        var updates = {};
        updates['/locations/' + newPostKey] = postData;

        return firebase.database().ref().update(updates);
    }


    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={{flex: 1}}>
                <MapView
                    ref={ref => this.map = ref}
                    style={{flex: 0.30}}
                    initialRegion={this.state.region}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    showsPointsOfInterest={true}
                    showsCompass={true}
                    showsScale={true}
                    showsBuildings={true}
                    showsTraffic={true}
                    showsIndoors={true}
                    showsIndoorLevelPicker={true}
                    provider="google"
                    onMapReady={(a) => {

                    }}
                    // onRegionChange={(region) => {
                    //     console.log('onRegionChange', region)
                    // }}
                    onRegionChangeComplete={(region) => {
                        console.log('onRegionChangeComplete', region)

                        this.setState({
                            marker: region
                        }, function() {
                            console.log(this.state.marker);
                        });
                    }}
                    onUserLocationChange={e => {
                        // console.log(e.nativeEvent)


                        if (!this.state.hadFirstAnimated) {
                            this.map.animateToCoordinate(e.nativeEvent.coordinate);
                            this.setState({
                                hadFirstAnimated: true
                            });
                        }

                    }}
                >
                    <Marker
                        coordinate={this.state.marker}
                        title={'marker.title'}
                        description={'marker.description'}
                    />
                </MapView>

                <Content style={{flex: 0.7, marginTop: 8, paddingHorizontal: 8}}>
                    <Form style={{flexDirection: 'column'}}>
                        <Text style={{fontSize: 18, color: '#575757'}}>Title</Text>
                        <Item>
                            <Input
                                onChangeText={(title) => this.setState({title})}
                                value={this.state.title}
                            />
                        </Item>

                        <Text style={{fontSize: 18, color: '#575757'}}>Opening Time</Text>
                        <RNPickerSelect
                            items={this.state.openTimeOptions}
                            onValueChange={(value) => {
                                this.setState({
                                    openTime: value,
                                });
                            }}
                            onUpArrow={() => {
                                this.inputRefs.name1.focus();
                            }}
                            onDownArrow={() => {
                                this.inputRefs.picker1.togglePicker();
                            }}
                            style={{ ...pickerSelectStyles }}
                            value={this.state.openTime}
                            ref={(el) => {
                                this.inputRefs.pickerOpenTime = el;
                            }}
                        />

                        <View style={{flexDirection: 'row'}}>
                            <TouchableOpacity
                                style={{flex: 0.5}}
                                onPress={() => {
                                    this.setState({isDateTimePickerFromVisible: true});
                                }}>
                                <View style={{padding: 2}}>
                                    <Text style={{fontSize: 18, color: '#575757'}}>From</Text>
                                    <Text style={{fontSize: 17, marginBottom: 4, marginTop: 4}}>{this.state.fromString}</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={{flex: 0.5}}
                                onPress={() => {
                                    this.setState({isDateTimePickerToVisible: true});
                                }}>
                                <View style={{padding: 2}}>
                                    <Text style={{fontSize: 18, color: '#575757'}}>To</Text>
                                    <Text style={{fontSize: 17, marginBottom: 4, marginTop: 4}}>{this.state.toString}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        <Text style={{fontSize: 18, color: '#575757'}}>Type</Text>
                        <RNPickerSelect
                            items={this.state.typeOptions}
                            onValueChange={(value) => {
                                this.setState({
                                    type: value,
                                });
                            }}
                            onUpArrow={() => {
                                this.inputRefs.name2.focus();
                            }}
                            onDownArrow={() => {
                                this.inputRefs.picker2.togglePicker();
                            }}
                            style={{ ...pickerSelectStyles }}
                            value={this.state.type}
                            ref={(el) => {
                                this.inputRefs.pickerType = el;
                            }}
                        />

                        <Text style={{fontSize: 18, color: '#575757'}}>WiFi</Text>
                        <RNPickerSelect
                            items={this.state.wifiOptions}
                            onValueChange={(value) => {
                                this.setState({
                                    wifi: value,
                                });
                            }}
                            onUpArrow={() => {
                                this.inputRefs.name3.focus();
                            }}
                            onDownArrow={() => {
                                this.inputRefs.picker3.togglePicker();
                            }}
                            style={{ ...pickerSelectStyles }}
                            value={this.state.wifi}
                            ref={(el) => {
                                this.inputRefs.pickerWifi = el;
                            }}
                        />

                        <Text style={{fontSize: 18, color: '#575757'}}>Parking</Text>
                        <RNPickerSelect
                            items={this.state.parkingOptions}
                            onValueChange={(value) => {
                                this.setState({
                                    parking: value,
                                });
                            }}
                            onUpArrow={() => {
                                this.inputRefs.name4.focus();
                            }}
                            onDownArrow={() => {
                                this.inputRefs.picker4.togglePicker();
                            }}
                            style={{ ...pickerSelectStyles }}
                            value={this.state.parking}
                            ref={(el) => {
                                this.inputRefs.parkingWifi = el;
                            }}
                        />

                        <Text style={{fontSize: 18, color: '#575757'}}>Picture</Text>
                        <PlaceCarousel data={['a': 1, 'a': 1, 'a': 1, 'a': 1, 'a': 1]} />

                        <Text style={{fontSize: 18, color: '#575757'}}>More Description</Text>
                        <Textarea
                            style={{marginBottom: 16}}
                            rowSpan={3}
                            bordered
                            value={this.state.description}
                        />

                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 0.5}}>
                                <Button rounded bordered success
                                    style={{alignSelf:'center', margin: 8}}
                                    onPress={() => {
                                        this.writeNewDiscovery();
                                    }}>
                                    <Text>Discover</Text>
                                </Button>
                            </View>

                            <View style={{flex: 0.5}}>
                                <Button rounded bordered danger
                                    style={{alignSelf:'center', margin: 8}}
                                    onPress={() => {
                                        
                                    }}>
                                    <Text>Cancel</Text>
                                </Button>
                            </View>
                        </View>

                        <Image source={this.state.avatarSource} />



                    </Form>
                </Content>

                <DateTimePicker
                    isVisible={this.state.isDateTimePickerFromVisible}
                    mode={'time'}
                    onConfirm={(date) => {
                        this.setState({
                            from: date,
                            fromString: moment(date).format('LT'),
                            isDateTimePickerFromVisible: false
                        });
                    }}
                    onCancel={() => {
                        this.setState({isDateTimePickerFromVisible: false});
                    }}
                />

                <DateTimePicker
                    isVisible={this.state.isDateTimePickerToVisible}
                    mode={'time'}
                    onConfirm={(date) => {
                        this.setState({
                            from: date,
                            toString: moment(date).format('LT'),
                            isDateTimePickerToVisible: false
                        });
                    }}
                    onCancel={() => {
                        this.setState({isDateTimePickerToVisible: false});
                    }}
                />

            </View>
        );
    }


}

const styles = {
    screen: {
        flex: 1,
    },
};

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        // borderWidth: 1,
        // borderColor: 'gray',
        // borderRadius: 4,
        // backgroundColor: 'white',
        color: 'black',
    },
});
