import React, { Component } from 'react';
import { AppRegistry, ActivityIndicator, TextInput, View, StyleSheet, Image } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Text } from 'native-base';
import { StackNavigator } from 'react-navigation';

import firebase from 'react-native-firebase';
import MapView from 'react-native-maps';

export default class StoryTab extends Component {

    constructor(props) {
        super(props);

        this.state = {
            hadFirstAnimated: false,
            region: {
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421
            },
            current: undefined
        }

        console.log(this.props)

    }

    componentWillMount() {
        var config = {
            apiKey: "AIzaSyDsll6qnvvoD2QqyVYomTP65ZOGWW5-qUQ",
            authDomain: "sithere-5db41.firebaseapp.com",
            databaseURL: "https://sithere-5db41.firebaseio.com",
            projectId: "sithere-5db41",
            storageBucket: "sithere-5db41.appspot.com",
            messagingSenderId: "872587675566"
          };
          firebase.initializeApp(config);


          firebase.database().ref('/locations').once('value').then(function(snapshot) {
              console.log(snapshot);
          });

    }

    componentDidMount() {
        console.log(firebase);

    }

    render() {
        // const { navigate } = this.props.navigation;

        return (
            <View style={{flex: 1}}>
                <MapView
                    ref={ref => this.map = ref}
                    style={{flex: 1}}
                    initialRegion={this.state.region}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    showsPointsOfInterest={true}
                    showsCompass={true}
                    showsScale={true}
                    showsBuildings={true}
                    showsTraffic={true}
                    showsIndoors={true}
                    showsIndoorLevelPicker={true}
                    provider="google"
                    onMapReady={(a) => {

                    }}
                    // onRegionChange={(region) => {
                    //     console.log('onRegionChange', region)
                    // }}
                    onRegionChangeComplete={(region) => {
                        // console.log('onRegionChangeComplete', region)
                        this.setState({
                            region: region
                        });
                    }}
                    onUserLocationChange={e => {
                        // console.log(e.nativeEvent)
                        this.setState({
                            current: e.nativeEvent.coordinate
                        });

                        if (!this.state.hadFirstAnimated) {
                            this.map.animateToCoordinate(e.nativeEvent.coordinate);
                            this.setState({
                                hadFirstAnimated: true
                            });
                        }

                    }}
                />

                <Button rounded bordered
                    style={{alignSelf:'center', margin: 8}}
                    onPress={() => {
                        navigate('DiscoverScreen', {current: this.state.current});
                    }}>
                    <Text>Discover</Text>
                </Button>

            </View>
        );
    }

}
