import React, { Component } from 'react';
import { AppRegistry, ActivityIndicator, TextInput, View, StyleSheet, Image, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Form, Item, Label, Input, Picker, Icon, Textarea } from 'native-base';
import { StackNavigator } from 'react-navigation';


import DateTimePicker from 'react-native-modal-datetime-picker';


import RNPickerSelect from 'react-native-picker-select';

import MapView, { Marker } from 'react-native-maps';
import firebase from 'react-native-firebase';

import moment from 'moment';

import PlaceCarousel from '../../components/PlaceCarousel';

import ImagePicker from 'react-native-image-picker';

import ImageResizer from 'react-native-image-resizer';


import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

const FirstRoute = () => (
  <View style={{ backgroundColor: '#ff4081' }} >
  <Text>FirstRoute</Text>
  </View>
);
const SecondRoute = () => (
  <View style={{ backgroundColor: '#673ab7' }} >
  <ScrollView>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  <Text>SecondRoute</Text>
  </ScrollView>
  </View>
);

export default class TestScreen extends Component {
    static navigationOptions = {
        title: 'TestScreen',
        // header: null
    };

    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            routes: [
              { key: 'first', title: 'First' },
              { key: 'second', title: 'Second' },
            ],
          };

    }

    componentWillMount() {


    }

    componentDidMount() {

    }




    render() {
        const { navigate } = this.props.navigation;

        return (
            <TabView
                navigationState={this.state}
                renderScene={SceneMap({
                    first: FirstRoute,
                    second: SecondRoute,
                })}
                onIndexChange={index => this.setState({ index })}
                initialLayout={{ width: Dimensions.get('window').width }}
                tabBarPosition={'bottom'}
            />
        );
    }


}
