import React, { Component } from 'react';
import { AppRegistry, ActivityIndicator, TextInput, View, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Card, CardItem, Left, Body, Right, Thumbnail, Icon } from 'native-base';

import Carousel, { ParallaxImage } from 'react-native-snap-carousel';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const IMAGE_SIZE = wp(75);

export default class PlaceCarousel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [...this.props.data, {action: 'button'}]
        }


    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    _renderItem ({item, index}) {
        console.log(item == {})
        return (
            <View>
            {
                item.action == undefined &&
                <View>
                    <Image
                        source={{uri: 'https://cdn.arstechnica.net/wp-content/uploads/2018/04/aveng-infinwar-logo.jpg'}}
                        style={{height: IMAGE_SIZE, width: IMAGE_SIZE, flex: 1, resizeMode: 'cover'}}
                    />
                </View>
            }
            {
                item.action != undefined &&
                <TouchableOpacity
                    onPress={() => {
                        ImagePicker.showImagePicker(this.options, (response) => {
                          console.log('Response = ', response);

                          if (response.didCancel) {
                            console.log('User cancelled image picker');
                          }
                          else if (response.error) {
                            console.log('ImagePicker Error: ', response.error);
                          }
                          else if (response.customButton) {
                            console.log('User tapped custom button: ', response.customButton);
                          }
                          else {
                            let source = { uri: response.uri };

                            // You can also display the image using data:
                            // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                            // ImageResizer.createResizedImage(response.uri, response.width, response.height, 'JPEG', 50)
                            // .then((response) => {
                            //     console.log(response);
                            //     // response.uri is the URI of the new image that can now be displayed, uploaded...
                            //     // response.path is the path of the new image
                            //     // response.name is the name of the new image with the extension
                            //     // response.size is the size of the new image
                            // }).catch((err) => {
                            //     console.log(err);
                            //     // Oops, something went wrong. Check that the filename is correct and
                            //     // inspect err to get more details.
                            // });




                            this.setState({
                              avatarSource: source
                            });
                          }
                        });
                    }}>
                    <Image
                        source={{uri: 'https://static1.squarespace.com/static/55bb726ce4b0d57edc0777a3/t/56bac1428a65e27aeda60e27/1455079981761/%2B.jpg'}}
                        style={{height: IMAGE_SIZE, width: IMAGE_SIZE, flex: 1, resizeMode: 'cover'}}
                    />
                </TouchableOpacity>
            }
            </View>
        );
    }

    render() {
        return (
            <View style={{marginVertical: 8}}>
                <Carousel
                    ref={(c) => { this._carousel = c; }}
                    data={this.state.data}
                    renderItem={this._renderItem}
                    sliderWidth={viewportWidth}
                    itemWidth={IMAGE_SIZE}
                    sliderHeight={IMAGE_SIZE}
                    itemHeight={IMAGE_SIZE}
                    hasParallaxImages={true}
                    inactiveSlideScale={0.90}
                    inactiveSlideOpacity={0.7}
                />
            </View>
        );
    }


}
